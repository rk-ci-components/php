# GitLab CI component for PHP

[![Latest Release](https://gitlab.com/rk-ci-components/php/-/badges/release.svg)](https://gitlab.com/rk-ci-components/php/-/releases)
[![pipeline status](https://gitlab.com/rk-ci-components/php/badges/master/pipeline.svg)](https://gitlab.com/rk-ci-components/php/-/commits/master)

This project implements a GitLab CI/CD template to build, test and analyse your PHP projects.

## Usage

This template can be used as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration).

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/rk-ci-components/php/php
    inputs:
      phpstan-disabled: true
```

## Global configuration

The semantic-release template uses some global configuration used throughout all jobs.

| Input / Variable                                      | Description                            | Default value |
|-------------------------------------------------------|----------------------------------------|---------------|
| `phpstan-disabled` / `PHPSTAN_DISABLED`               | Disables the PHPStan job.              | `false`       |
| `phpcsfixer-disabled` / `PHPCSFIXER_DISABLED`         | Disables the php-cs-fixer job.         | `false`       |
| `phpunit-disabled` / `PHPUNIT_DISABLED`               | Disables the PHPUnit job.              | `false`       |
| `composer-audit-disabled` / `COMPOSER_AUDIT_DISABLED` | Disables the Composer audit job.       | `false`       |

## Jobs

### `phpstan` job

This job performs a [PHPStan](https://phpstan.org/) analysis of your code.

It is bound to the `test` stage, and is **enabled by default**.

### `php-cs-fixer` job

This job performs a [PHP-CS-Fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer) analysis of your code.

It is bound to the `test` stage, and is **enabled by default**.

### `php-unit` job

This job performs [PHPUnit](https://docs.phpunit.de/) tests.

It is bound to the `test` stage, and is **enabled by default**.

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report                                 | Format                                                                       | Usage             |
|----------------------------------------| ---------------------------------------------------------------------------- | ----------------- |
| `./reports/php-test.xunit.xml`         | [xUnit](https://en.wikipedia.org/wiki/XUnit) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) & [SonarQube integration](https://docs.sonarqube.org/latest/analysis/test-coverage/test-execution-parameters/#header-7) |
| `./reports/php-coverage.cobertura.xml` | [Cobertura XML](https://gcovr.com/en/stable/output/cobertura.html) coverage report | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscoverage_report) |
| `./reports/php-coverage.clover.xml`    | [Clover XML](https://openclover.org/) coverage report | [SonarQube integration](https://docs.sonarqube.org/latest/analysis/test-coverage/test-coverage-parameters/#header-9) |


### `php-composer-audit` job

This job performs a vulnerability scan in your dependencies with [`composer audit`](https://getcomposer.org/doc/03-cli.md#audit).

It is bound to the `test` stage, and is **enabled by default**.

### `php-sbom` job

This job generates a [SBOM](https://cyclonedx.org/) file listing installed packages using [@cyclonedx/cyclonedx-php](https://github.com/CycloneDX/cyclonedx-php-composer).

It is bound to the `test` stage, **enabled by default**, and uses the following variables:

| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `sbom-disabled` / `PHP_SBOM_DISABLED` | Set to `true` to disable this job | _none_ |
| `sbom-version` / `PHP_SBOM_VERSION` | The version of @cyclonedx/cyclonedx-php used to emit SBOM | _none_ (uses latest) |
