## [1.1.3](https://gitlab.com/rk-ci-components/php/compare/v1.1.2...1.1.3) (2024-04-02)


### Bug Fixes

* **release:** improved releaserc ([1671938](https://gitlab.com/rk-ci-components/php/commit/1671938369e5001c9bc76edccd0180cfea3eac85))
